
import firebase from 'firebase/app';
import "firebase/messaging";

const firebaseConfig = {
    apiKey: "AIzaSyCeXVouY75AO0qdVmWL_IJO8GjBpNVL-k0",
    authDomain: "fir-cloud-messaging-e7a93.firebaseapp.com",
    projectId: "fir-cloud-messaging-e7a93",
    storageBucket: "fir-cloud-messaging-e7a93.appspot.com",
    messagingSenderId: "340561113732",
    appId: "1:340561113732:web:6169c70d89988b99aec8d1"
  };

  firebase.initializeApp(firebaseConfig)

  const messaging = firebase.messaging()
  const vapidKey = "BCdMFcijhDEGYiFD7DL-CyXl3WsmwiCytppoDlTfaN9lbThVftrSCtf6l8sGmMypMyLCrGtNldQ_DtCdK_QiCa4"

  export const getToken = async (setTokenFound)=>{
    let currentToken = ""
    try {
        currentToken = await messaging.getToken({vapidKey: vapidKey});
        if(currentToken){
            setTokenFound(true)
        }
        else{
            setTokenFound(false)
        }
    } catch (error) {
        console.log(error)
    }
    return currentToken
  }

  export const onMessageListener = () =>
  new Promise((resolve) => {
    messaging.onMessage((payload) => {
      resolve(payload);
    });
  });