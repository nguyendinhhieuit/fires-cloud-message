/* eslint-disable no-undef */

import React from 'react';
import './App.css';
import { useEffect } from 'react';

import Notification from "./components/Notifications/Notifications"
 
function App() {

 
  return (
    <div className="App">
      <Notification></Notification>
    </div>
  );
}

export default App;
