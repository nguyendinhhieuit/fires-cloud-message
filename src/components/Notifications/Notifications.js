import React, { useState, useEffect } from "react";
import { getToken } from "../../firebase";
import firebase from 'firebase/app';

const Notifications = (props) => {
  const [isTokenFound, setTokenFound] = useState(false);

  console.log("Token found", isTokenFound);


  const messaging = firebase.messaging()
  messaging.onMessage((res) => {
    console.log(res) 
  })

  const tokenFunc = async ()=>{
    const data = await getToken(setTokenFound);
    if (data) {
      console.log("Token is", data);
    }
    return data;
  }
  // To load once
  useEffect(() => {
    tokenFunc()
  }, [setTokenFound]);

  const sendMessage = ()=>{
    const myHeaders = new Headers();
myHeaders.append("Content-Type", "application/json");
myHeaders.append("Authorization", "key=AAAAT0sGMoQ:APA91bEd8pv7opeMIGX4hjUkQ68XNT003VIT6qcdvxIyUf-5wxcF9IWC00_gw2Gf50Jxg8ZI2_kO3DUyN9pefBS_j4ikY_WlynQrA0MUZfJXnfVShY-SUT4nAFZp3s3RJ4iRmK4DfMP_");

const raw = JSON.stringify({
  "to": "eEpanV40mopyn85idV54FX:APA91bHE-FC7I-y493YKLvZJWadSbkLg-YN1N5YQIFSRQsqGi30D14hygro0DqgfaUIICSa8wxp9HiIDqONJBwkRnvOMlON_ImZvKvocJO0iil7xGKuRpcdnT2GitPjJl773lUS3izH1",
  "notification": {
    "title": "Check this Mobile (title)",
    "body": "Rich Notification testing (body)",
    "icon": "https://i.pinimg.com/474x/37/b3/3c/37b33c63d7da9cd42dbc4f2682c37ecb.jpg",
    "sound": "Tri-tone"
  }
});

const requestOptions = {
  method: 'POST',
  headers: myHeaders,
  body: raw,
  redirect: 'follow'
};

fetch("https://fcm.googleapis.com/fcm/send", requestOptions)
  .then(response => response.json())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
  }
  return (
  <>
    <button onClick={sendMessage}>Send </button>
  </>
  )
};

Notifications.propTypes = {};

export default Notifications;
